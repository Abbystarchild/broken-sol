// function Person(first, last, age, gender, interests) {

//     // property and method definitions
//     this.name = {
//         'first': first,
//         'last': last
//     };
//     this.age = age;
//     this.gender = gender;
//     //...see link in summary above for full definition
// }


function NPC(first, last, age, profession, lifestyle, interests, quote) {
    this.name = {
        'first': first,
        'last': last
    };
    this.age = age //random number if value is less than 18
    this.profession = profession
    this.lifestyle = lifestyle
    this.interests = interests
    this.quote = quote
}
let getNPC = function() {
    // grabs random data for NPC spawn
}
let getPlayer = function() {
    //grabs player info for player spawn
}
let getMob = function() {
    // grabs mob data for spawn
}
let loadSave = function() {
    // load game
}
let saveGame = function() {
    // saves the player info to a const until loaded.
}
let loadMap = function() {
    // loads area data 
}
let mapData = {
    //mapdata here
}

let playerControls = function() {
    // contains movement methods        
}
let playerInventory = {
    //player inventory here
}
let getInventory = function() {
    //grabs inventory data
}
let abby = new NPC("Abby", "Starchild", 32, "Lunarium Hoarder", "Hermit", "Science", "I barely remember to eat, I love these gems so much.")



// Steps to prepare for success
// 1. Setup basic functionality - in no more than 4 steps what does the function do in relation to the object of the game ?
// 2. Spend at least one hour each night dedicated to working on this project.
// 3. Understand the fundamentals and go over old work and demos daily!
// 4. Do a kata, You maY not like it. But it forces you to use the stuff yOu want to remember anyway!


// Ideas, Use objects and arrays and random numbers to generate random NPCs with constructors so that the data does not have to be typed out by hand for each name in the name array.
// Same can be done for all large groups of data, such as the keywords and the tile sets used to generate each level. Same principal as the maze assignment but I need to think much bigger and make effecient use of data structures and guard clauses in the algorithms I need to write to make each tileset cohesively mesh with the other tile sets used when a map stylized by the random keywords. Perhaps use combinations of each possible tile set but setting the tiles sets to have a set amount of physical attributes or visible but non corporeal effects which can be identified by the style applied to each keyword array.
// Find out how to use tilesets in javascript
// find out how to use animations in CSS
// master OOP and be sure to set up your program so you can learn from it if you come back to it after a while and forget something. leave comments